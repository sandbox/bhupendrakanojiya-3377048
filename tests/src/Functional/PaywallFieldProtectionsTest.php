<?php

namespace Drupal\Tests\entity_field_permissions\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests paywall.
 *
 * @group paywall field protection
 */
class PaywallFieldProtectionsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['entity_field_permissions'];

  /**
   * A user with the 'Administer entity_field_permissions' permission.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create admin user.
    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'administer nodes',
      'create page content',
      'edit any page content',
      'access content',
    ]);

  }

  /**
   * Test paywall icon on the basic page content.
   */
  public function testPaywallIcon() {

    // User Login.
    $this->drupalLogin($this->adminUser);
    // Create a sample page.
    $title_value = $this->randomGenerator->word(10);
    $body_value = $this->randomGenerator->sentences(10);
    $edit = [
      'title[0][value]' => $title_value,
      'body[0][value]' => $body_value,
    ];
    $this->drupalGet('node/add/page');
    $this->submitForm($edit, 'Save');

    // Edit the page.
    $node = $this->getNodeByTitle($title_value);
    $this->drupalGet('node/' . $node->id() . '/edit');
    $protect_role = [
      'options[content_editor]' => "content_editor",
    ];
    $this->drupalGet("get-user-details");
    $this->submitForm($protect_role, 'op');
  }

  /**
   * Test paywall icon on the basic page content.
   */
  public function testImageProtection() {

    // User Login.
    $this->drupalLogin($this->adminUser);
    // Create a sample page.
    $title_value = $this->randomGenerator->word(10);
    $body_value = $this->randomGenerator->sentences(10);
    $edit = [
      'title[0][value]' => $title_value,
      'body[0][value]' => $body_value,
    ];
    $this->drupalGet('node/add/page');
    $this->submitForm($edit, 'Save');

    // Edit the page.
    $node = $this->getNodeByTitle($title_value);
    $this->drupalGet('node/' . $node->id() . '/edit');
    $image_option = [
      'image_option' => 'lock_icon',
    ];
    $url = '/get-user-details';
    $query_parameters = ['is_image' => 1];
    $this->drupalGet($url, ['query' => $query_parameters]);
    $this->submitForm($image_option, 'op');
    $this->assertSession()->statusCodeEquals(200);
  }

}
