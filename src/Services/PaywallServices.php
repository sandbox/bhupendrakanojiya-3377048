<?php

namespace Drupal\entity_field_permissions\Services;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\file\Entity\File;
use Drupal\user\Entity\User;

/**
 * Service class to add remove and delete paywall data.
 */
class PaywallServices {

  /**
   * The database connection object.
   */
  protected Connection $connection;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
    // $this->paywalloneService = $paywalloneService;
  }

  /**
   * Check access against roles.
   *
   * @param int $entity_id
   *   Entity id of the node.
   * @param string $field_name
   *   Field name to check the access.
   *
   * @return mixed
   *   Return selected roles.
   */
  public function getAccessibleFieldRoles(int $entity_id = NULL, string $field_name = NULL) {
    $selected_roles = [];
    try {
      $query = $this->connection->select('entity_field_permissions', 's');
      $query->condition('s.field_name', "%" . $field_name . "%", 'LIKE');
      $orGroup = $query->orConditionGroup();
      $orGroup->condition('s.entity_id', $entity_id);
      $query->condition($orGroup);
      $query->fields('s', ['roles']);
      $result = $query->execute();
      while ($res = $result->fetchAssoc()) {
        $arr = explode(",", $res['roles']);
        foreach ($arr as $val) {
          $selected_roles[$val] = $val;
        }
      }
      return $selected_roles;
    }
    catch (DatabaseExceptionWrapper $e) {
      return $e->getMessage();
    }
  }

  /**
   * Check access against roles.
   *
   * @param int $entity_id
   *   Entity id of the node.
   * @param string $field_name
   *   Field name to check the access.
   *
   * @return array
   *   Return image options.
   */
  public function getImageOptions($entity_id, $field_name): array {
    $selected_image = [];
    $query = $this->connection->select('entity_field_permissions', 's');
    $query->condition('s.field_name', "%" . $field_name . "%", 'LIKE');
    $orGroup = $query->orConditionGroup();
    $orGroup->condition('s.entity_id', $entity_id);
    $query->condition($orGroup);
    $query->fields('s', ['image_option']);
    $result = $query->execute();
    while ($res = $result->fetchAssoc()) {
      if ($res['image_option'] != NULL) {
        $selected_image[$res['image_option']] = $res['image_option'];
      }
    }
    return $selected_image;
  }

  /**
   * Check access against roles.
   *
   * @param string $field
   *   Field name to check the access.
   * @param int $id
   *   Node id of the field.
   *
   * @return mixed
   *   Return error response.
   */
  public function checkFieldPaywall($field, $id) {
    $uid = \Drupal::service('solar_bff.scout_services')->isValidCsrfToken();
    if (empty($uid)) {
      $uid = $this->getPwUserId();
    }
    $user_roles = [];
    if (!empty($uid)) {
      $user = User::load($uid);
      $user_roles = $user->getRoles();
      // $user_country = $this->paywalloneService->getUserCountry($user);
    }
    $result = $this->userFieldAccess($user_roles, $id);
    // $result = user_field_access($user_roles, $id, $user_country);
    if (in_array($field, $result['fields'])) {
      $field_response = [
        'error_code' => 410,
        'error_msg' => 'You have no permission to access this page.',
      ];
    }
    if (array_key_exists($field, $result['fields_image'])) {
      $field_response['image_option'] = $result['fields_image'][$field];
    }
    if (array_key_exists($field, $result['granted_roles'])) {
      $field_response['roles'] = $result['granted_roles'][$field];
    }
    if (array_key_exists($field, $result['country'])) {
      $field_response['country'] = $result['country'][$field];
    }
    return $field_response;
  }

  /**
   * Method to add permssion against field.
   *
   * @param array $roles
   *   Roles of current user.
   * @param mixed $country
   *   Future implementation.
   * @param array $values
   *   Values to save in table.
   *
   * @return {:mixed}
   *   Return data or error object.
   */
  public function insertPaywallData($roles, $country, $values) {
    try {
      $image_option = $values['image_option'] ?? '';
      $current_user = \Drupal::currentUser();
      $current_user_id = (!empty($current_user)) ? $current_user->id() : NULL;
      $query = $this->connection->insert('entity_field_permissions')->fields([
        'field_name' => $values['field_name'],
        'entity_id' => $values['entity_id'],
        'custom_token' => $values['custom_token'],
        'image_option' => $image_option,
        'roles' => $roles,
        'country' => $country,
        'exceptional_content' => $values['exceptional_content'],
        'created_by' => $current_user_id,
      ]);
      $data = $query->execute();
      return $data;
    }
    catch (DatabaseExceptionWrapper $e) {
      return $e->getMessage();
    }
  }

  /**
   * Method to update permssion against field.
   *
   * @param array $roles
   *   Roles of current user.
   * @param mixed $country
   *   Future implementation.
   * @param array $values
   *   Values to save in table.
   *
   * @return {:mixed}
   *   Return data or error object.
   */
  public function updatePaywallData($roles, $country, $values) {
    $current_user = \Drupal::currentUser();
    $current_user_id = (!empty($current_user)) ? $current_user->id() : '';
    if (empty($roles) && empty($country) && empty($values['exceptional_content'])) {
      return $this->deletePaywallData($values);
    }
    try {
      $image_option = $values['image_option'] ?? '';
      $query = $this->connection->update('entity_field_permissions')->fields([
        'entity_id' => $values['entity_id'],
        'roles' => $roles,
        'image_option' => $image_option,
        'country' => $country,
        'exceptional_content' => $values['exceptional_content'],
        'updated_at' => date("Y-m-d H:i:s", time() - date("Z")),
        'updated_by' => $current_user_id,
      ]);
      if ($values['field_name']) {
        $query->condition('field_name', $values['field_name']);
      }
      if ($values['entity_id']) {
        $query->condition('entity_id', $values['entity_id']);
      }
      elseif ($values['custom_token']) {
        $query->condition('custom_token', $values['custom_token']);
      }
      $data = $query->execute();
      return $data;
    }
    catch (DatabaseExceptionWrapper $e) {
      return $e->getMessage();
    }
  }

  /**
   * Method to update custom token.
   *
   * @param int $entity_id
   *   Roles of current user.
   * @param mixed $custom_token
   *   Future implementation.
   *
   * @return void
   *   Return nothing.
   */
  public function updatePaywallCustomToken($entity_id, $custom_token): void {
    $conn = $this->connection;
    $query = $conn->update('entity_field_permissions')->fields([
      'entity_id' => $entity_id,
      'custom_token' => NULL,
    ]);
    $query->condition('custom_token', $custom_token);
    $query->execute();
  }

  /**
   * Function to get user access fields lists.
   *
   * @param array $user_roles
   *   User roles.
   * @param int $entity_id
   *   Entity Id.
   *
   * @return array
   *   Return array of response.
   */
  public function userFieldAccess($user_roles, $entity_id) {
    $query = $this->connection->select('entity_field_permissions', 's');
    $query->condition('s.entity_id', $entity_id);
    $query->fields('s', ['roles', 'field_name', 'image_option']);
    $result = $query->execute();
    $res_arr = $roles_res = $image_res = [];
    while ($res = $result->fetchAssoc()) {
      if ((empty($user_roles)) || (!empty($user_roles) && empty(array_intersect(explode(',', $res['roles']), $user_roles)))) {
        $res_arr[] = $res['field_name'];
        if ($res['image_option']) {
          if ($res['image_option'] == 'other') {
            if ($cache = \Drupal::cache()->get('paywall_other_image_taget_id')) {
              $target_id = $cache->data;
            }
            else {
              $image_config = \Drupal::config('custom_site_configuration.settings');
              $data = $image_config->get('content_protection_image_field_default_image');
              if ($data) {
                $target_id = $data[0];
              }
              $res = \Drupal::cache()->set('paywall_other_image_taget_id', $target_id, Cache::PERMANENT);
            }
            $image_res[$res['field_name']] = [
              'option' => $res['image_option'] ,
              "image_style" => $this->getImageStyles($target_id),
            ];

          }
          else {
            $image_res[$res['field_name']] = $res['image_option'];
          }
        }
        if ($res['roles']) {
          $roles_res[$res['field_name']] = $this->getRoleLabel($res['roles']);
        }
      }
    }
    $response['fields'] = $res_arr;
    $response['fields_image'] = $image_res;
    $response['granted_roles'] = $roles_res;
    return $response;
  }

  /**
   * Function to delete paywall data.
   *
   * @param array $values
   *   Parameters for conditions.
   *
   * @return {:mixed}
   *   Return data or error object.
   */
  public function deletePaywallData($values) {
    try {
      $query = $this->connection->delete('entity_field_permissions');
      if ($values['field_name']) {
        $query->condition('field_name', $values['field_name']);
      }
      if ($values['entity_id']) {
        $query->condition('entity_id', $values['entity_id']);
      }
      elseif ($values['custom_token']) {
        $query->condition('custom_token', $values['custom_token']);
      }
      $data = $query->execute();
      return $data;
    }
    catch (DatabaseExceptionWrapper $e) {
      return $e->getMessage();
    }
  }

  /**
   * Function to get all roles.
   *
   * @param mixed $protected_roles
   *   Roles which restricted for a field.
   *
   * @return string
   *   Return string of all available roles for the field.
   */
  public function getRoleLabel($protected_roles) {
    $roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
    $roles_labels = [];
    foreach ($roles as $key => $role) {
      if ($key != 'administrator') {
        $roles_labels[$key] = $role->label();
      }
    }

    $protected_roles = explode(",", $protected_roles);
    foreach ($protected_roles as $key => $value) {
      $protected_roles[$key] = $roles_labels[$value];
    }
    return implode(",", $protected_roles);
  }

  /**
   * Function to get image style.
   *
   * @param int $target_id
   *   Target id of file.
   *
   * @return array
   *   Return array of image styles.
   */
  public function getImageStyles($target_id) {
    $file = File::load($target_id);
    $style_16_9_large = \Drupal::entityTypeManager()->getStorage('image_style')->load('16_9_large');
    $style_16_9_large_2x = \Drupal::entityTypeManager()->getStorage('image_style')->load('16_9_large_2x');
    $style_16_9_medium = \Drupal::entityTypeManager()->getStorage('image_style')->load('16_9_medium');
    $style_16_9_medium_2x = \Drupal::entityTypeManager()->getStorage('image_style')->load('16_9_medium_2x');
    $style_16_9_small = \Drupal::entityTypeManager()->getStorage('image_style')->load('16_9_small');
    $style_16_9_small_2x = \Drupal::entityTypeManager()->getStorage('image_style')->load('16_9_small_2x');
    $style_1_1_large = \Drupal::entityTypeManager()->getStorage('image_style')->load('1_1_large');
    $style_1_1_large_2x = \Drupal::entityTypeManager()->getStorage('image_style')->load('1_1_large_2x');
    $style_1_1_medium = \Drupal::entityTypeManager()->getStorage('image_style')->load('1_1_medium');
    $style_1_1_medium_2x = \Drupal::entityTypeManager()->getStorage('image_style')->load('1_1_medium_2x');
    $style_1_1_small = \Drupal::entityTypeManager()->getStorage('image_style')->load('1_1_small');
    $style_1_1_small_2x = \Drupal::entityTypeManager()->getStorage('image_style')->load('1_1_small_2x');
    $style_4_3_large = \Drupal::entityTypeManager()->getStorage('image_style')->load('4_3_large');
    $style_4_3_large_2x = \Drupal::entityTypeManager()->getStorage('image_style')->load('4_3_large_2x');
    $style_4_3_medium = \Drupal::entityTypeManager()->getStorage('image_style')->load('4_3_medium');
    $style_4_3_medium_2x = \Drupal::entityTypeManager()->getStorage('image_style')->load('4_3_medium_2x');
    $style_4_3_small = \Drupal::entityTypeManager()->getStorage('image_style')->load('4_3_small');
    $style_4_3_small_2x = \Drupal::entityTypeManager()->getStorage('image_style')->load('4_3_small_2x');
    $style_hero = \Drupal::entityTypeManager()->getStorage('image_style')->load('hero');
    $style_hero_2x = \Drupal::entityTypeManager()->getStorage('image_style')->load('hero_2x');
    $style_teaser_wide = \Drupal::entityTypeManager()->getStorage('image_style')->load('teaser_wide');
    $style_teaser_wide_2x = \Drupal::entityTypeManager()->getStorage('image_style')->load('teaser_wide_2x');

    $image_grid_list[] = [
      "src" => \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri()),
      "alt" => $file->getFilename(),
      "caption" => $file->getFilename(),
      "image_style_uri" => [
        "16_9_large" => $style_16_9_large->buildUrl($file->getFileUri()),
        "16_9_large_2x" => $style_16_9_large_2x->buildUrl($file->getFileUri()),
        "16_9_medium" => $style_16_9_medium->buildUrl($file->getFileUri()),
        "16_9_medium_2x" => $style_16_9_medium_2x->buildUrl($file->getFileUri()),
        "16_9_small" => $style_16_9_small->buildUrl($file->getFileUri()),
        "16_9_small_2x" => $style_16_9_small_2x->buildUrl($file->getFileUri()),
        "1_1_large" => $style_1_1_large->buildUrl($file->getFileUri()),
        "1_1_large_2x" => $style_1_1_large_2x->buildUrl($file->getFileUri()),
        "1_1_medium" => $style_1_1_medium->buildUrl($file->getFileUri()),
        "1_1_medium_2x" => $style_1_1_medium_2x->buildUrl($file->getFileUri()),
        "1_1_small" => $style_1_1_small->buildUrl($file->getFileUri()),
        "1_1_small_2x" => $style_1_1_small_2x->buildUrl($file->getFileUri()),
        "4_3_large" => $style_4_3_large->buildUrl($file->getFileUri()),
        "4_3_large_2x" => $style_4_3_large_2x->buildUrl($file->getFileUri()),
        "4_3_medium" => $style_4_3_medium->buildUrl($file->getFileUri()),
        "4_3_medium_2x" => $style_4_3_medium_2x->buildUrl($file->getFileUri()),
        "4_3_small" => $style_4_3_small->buildUrl($file->getFileUri()),
        "4_3_small_2x" => $style_4_3_small_2x->buildUrl($file->getFileUri()),
        "hero" => $style_hero->buildUrl($file->getFileUri()),
        "hero_2x" => $style_hero_2x->buildUrl($file->getFileUri()),
        "teaser_wide" => $style_teaser_wide->buildUrl($file->getFileUri()),
        "teaser_wide_2x" => $style_teaser_wide_2x->buildUrl($file->getFileUri()),
      ],
    ];

    return $image_grid_list;
  }

}
