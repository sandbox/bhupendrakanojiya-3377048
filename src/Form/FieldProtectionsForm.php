<?php

namespace Drupal\entity_field_permissions\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_field_permissions\Services\PaywallServices;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class fieldProtectionForm.
 */
class FieldProtectionsForm extends FormBase {

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The RoleServices service.
   */
  protected PaywallServices $paywallServices;

  /**
   * The requestStack service.
   */
  protected RequestStack $requestack;

  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
    PaywallServices $paywallServices,
    RequestStack $request_tack) {
    $this->entityTypeManager = $entity_type_manager;
    $this->paywallServices = $paywallServices;
    $this->requeststack = $request_tack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('entity_type.manager'),
          $container->get('entity_field_permissions.paywall_services'),
          $container->get('request_stack'),
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "field_level_permissions_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* getting the user roles */
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    $labels = [];
    foreach ($roles as $key => $role) {
      if ($key != 'administrator') {
        $labels[$key] = $role->label();
      }
    }
    $nid = $this->requeststack->getCurrentRequest()->query->get('nid');
    $fid = $this->requeststack->getCurrentRequest()->query->get('fid');
    if ($this->requeststack->getCurrentRequest()->hasSession()) {
      $session = $this->requeststack->getCurrentRequest()->getSession();
      $custom_form_token = $session->get('custom_form_token');
      $is_image = $this->requeststack->getCurrentRequest()->query->get('is_image');
      if (empty($nid)) {
        $nid = NULL;
      }
      // Getting selected roles.
      $values['custom_token'] = $custom_form_token;
    }
    $values['entity_id'] = $nid;
    $values['field_name'] = $fid;
    $paywall_data = $this->paywallServices->selectPaywallData($values);
    // Getting selected image option.
    $selected_image = $this->paywallServices->getImageOptions($nid, $fid);
    $form['#attached']['library'][] = "entity_field_permissions/FieldProtectionsForm";
    $form['element'] = [
      '#type' => 'markup',
      '#markup' => "<div class='success'></div><div class='paywall_error'></div>",
    ];
    $form['field_name'] = [
      '#type' => 'hidden',
      '#title' => 'Field Name',
      '#value' => $fid,
      '#disabled' => TRUE,
    ];
    $form['entity_id'] = [
      '#type' => 'hidden',
      '#title' => 'Entity ID',
      '#value' => $nid,
      '#disabled' => TRUE,
    ];
    $form['custom_token'] = [
      '#type' => 'hidden',
      '#default_value' => $custom_form_token,
      '#disabled' => TRUE,
    ];
    if ($is_image) {
      $image_options = [
        'grey_overley' => 'Grey overley',
        'lock_icon' => 'Lock Icon Overley',
        'hide_image' => 'Hide the image',
        'other' => 'Any other pre-defined custom image',
      ];
      $form['image_option'] = [
        '#type' => 'select',
        '#title' => 'Please Select image action',
        '#options' => $image_options,
        '#default_value' => $selected_image,
      ];
    }
    $form['options'] = [
      '#type' => 'checkboxes',
      '#title' => 'Please Select Roles',
      '#options' => $labels,
      '#default_value' => $paywall_data['roles'],
    ];

    $custom_id_config = \Drupal::config('solar_azure_connect.settings');
    $enable_global_site = $custom_id_config->get('enable_global_site');
    if ($enable_global_site) {
      $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
      $country_tree = $term_storage->loadTree('country', 0, 2, TRUE);
      $country_list[] = $this->t('-None-');
      foreach ($country_tree as $term) {
        if (!empty($term_storage->loadParents($term->id()))) {
          $country_list[$term->getName()] = $term->getName();
        }
      }
      $form['country'] = [
        '#type' => 'select',
        '#title' => $this->t('Please Select Country'),
        '#options' => $country_list,
        '#default_value' => $paywall_data['country'],
        '#empty_value' => $this->t('-None-'),
        '#multiple' => TRUE,
      ];
    }
    else {
      $form['country'] = [
        '#type' => 'hidden',
        '#value' => [],
      ];
    }
    $show_excep_checkbox = 1;
    if (!empty($custom_form_token) && strpos($custom_form_token, 'node_pim_csv_product') !== FALSE) {
      $show_excep_checkbox = 0;
    }
    if ($fid != 'enable_paywall_block_level' && $show_excep_checkbox == 1) {
      $disable_anon_content_val = NULL;
      if ($paywall_data['exceptional_content'] == 1) {
        $disable_anon_content_val = TRUE;
      }
      $form['exceptional_content'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Exceptional Content'),
        '#description' => $this->t('If checked and "Show all anonymous content" is enabled in the parent page and field is unprotected, this content will be visible for authenticated user.'),
        '#default_value' => $disable_anon_content_val,
      ];
    }
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Submit',
      '#ajax' => [
        'callback' => '::setAjaxSubmit',
        'disable-refocus' => TRUE,
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function setAjaxSubmit(array &$form, FormStateInterface $form_state) {

    /* custom field roles entry start */
    $roles = implode(",", array_filter($form_state->getValue('options'))) ?? '';
    $country = implode(",", array_filter($form_state->getValue('country'))) ?? '';
    $values = $form_state->getValues();
    $excep_content = $values['exceptional_content'];
    $result = $this->paywallServices->selectPaywallData($values);
    if (!empty($result)) {
      $query_result = $this->paywallServices->updatePaywallData($roles, $country, $values);
    }
    else {
      if ($roles || $country || $excep_content == 1) {
        $query_result = $this->paywallServices->insertPaywallData($roles, $country, $values);
      }

      if (empty($roles) && empty($country) && empty($excep_content) && empty($result)) {
        $response = new AjaxResponse();
        $response->addCommand(new HtmlCommand(".paywall_error", 'Please select roles or countries or exceptional content.'));
        return $response;
      }
    }
    if ($query_result) {
      /* custom field roles entry end */
      $response = new AjaxResponse();
      $response->addCommand(new HtmlCommand(".success", 'Form submitted successfully!'));
      $response->addCommand(new CloseModalDialogCommand());
      return $response;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(&$form, FormStateInterface $form_state): void {

  }

}
